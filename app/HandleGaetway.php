<?php

namespace App;

/**
 * Executes request
 *
 * @param       string      $url                Url for payment method
 * @param       array       $requestFields      Request data fields
 *
 * @return      array                           Host response fields
 *
 * @throws      RuntimeException                Error while executing request
 */

/**
 * Account details
 *
 */

//// dev
//$gate = 'sandbox';
//$groupId = isset($_POST['sd3']) && $_POST['sd3'] === 'sd3' ? '235' : '236';
//$login = 'CanadaTimeTestControl';
//$merchantControl = '74E884D8-15FC-4E3E-A931-57A5D4390923';
//$order = 'CanadaTimeControl';

// prod

class HandleGaetway
{
    public function sendRequest($url, array $requestFields)
    {
        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER         => 0,
            CURLOPT_USERAGENT      => 'Fibonatix-Client/1.0',
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST           => 1,
            CURLOPT_RETURNTRANSFER => 1
        ));

        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($requestFields));

        $response = curl_exec($curl);

        if (curl_errno($curl)) {
            $error_message  = 'Error occurred: ' . curl_error($curl);
            $error_code     = curl_errno($curl);
        } elseif (curl_getinfo($curl, CURLINFO_HTTP_CODE) != 200) {
            $error_code     = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $error_message  = "Error occurred. HTTP code: '{$error_code}'";
        }

        curl_close($curl);

        if (! empty($error_message)) {
            return [$error_message, $error_code];
            // throw new RuntimeException($error_message, $error_code);
        }

        if (empty($response)) {
            return ['Host response is empty'];
            //throw new RuntimeException('Host response is empty');
        }
        // echo $response;
        $responseFields = array();

        parse_str($response, $responseFields);
        return $responseFields;
    }


    public function getCisProps($_3d = false){
        return [
            'group_id' => isset($_POST['sd3']) && $_POST['sd3'] === 'sd3' ? '1724' : '1724',
            'login' => 'CanadaCIS',
            'order' => 'canadacis',
            'merchantControl' => '7D4F3549-A659-4DEA-A8DC-804593A32800'
        ];
    }

    public function getCvsProps($_3d = false){ 

        return [
            'group_id' => ( isset($_POST['sd3']) && $_POST['sd3'] === 'sd3') || $_3d ? '1618' : '1618',
            'login' => 'Canada_VS',
            'order' => 'canadavisaservices',
            'merchantControl' => 'DB4B0209-A908-4977-A675-F993F77020D1'
        ];
    }

    public function signString($s, $merchantControl)
    {
        return sha1($s . $merchantControl);
    }

    /**
     * Signs payment (sale/auth/transfer) request
     *
     * @param 	array		$requestFields		request array
     * @param	string		$endpointOrGroupId	endpoint or endpoint group ID
     * @param	string		$merchantControl	merchant control key
     */
    public function signPaymentRequest($requestFields, $endpointOrGroupId, $merchantControl)
    {
        $base = '';
        $base .= $endpointOrGroupId;
        $base .= $requestFields['client_orderid'];
        $base .= $requestFields['amount'] * 100;
        $base .= $requestFields['email'];

        return $this->signString($base, $merchantControl);
    }

    /**
     * Signs status request
     *
     * @param 	array		$requestFields		request array
     * @param	string		$login			merchant login
     * @param	string		$merchantControl	merchant control key
     */
    public function signStatusRequest($requestFields, $login, $merchantControl)
    {
        $base = '';
        $base .= $login;
        $base .= $requestFields['client_orderid'];
        $base .= $requestFields['orderid'];

        return $this->signString($base, $merchantControl);
    }


    public function signAccountVerificationRequest($requestFields, $endpointOrGroupId, $merchantControl)
    {
        $base = '';
        $base .= $endpointOrGroupId;
        $base .= $requestFields['client_orderid'];
        $base .= $requestFields['email'];
        return $this->signString($base, $merchantControl);
    }
}
