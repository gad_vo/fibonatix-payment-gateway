<?php

namespace App;

use App\HandleGaetway;


// include 'HandleGaetway.php';

// include "assets/functions/fibonatix.php";
// use App\fibonatix\PaymentRegister;
/**
 * Custom payment
 *
 * @param       string      $url                Url for payment method
 * @param       array       $requestFields      Request data fields
 *
 * @return      array                           Host response fields
 *
 */

// print_r((new PaymentRegister));
//file_put_contents( 'outputtest.json', json_encode($_POST) ); // for dev testing

// 

class Payment
{
    protected $ReqGateway;
    protected $brand = [
        'cvs' => false,
        'cis' => false
    ];

    //  controll if payment done through  3d autharizetion
    protected $_3d = false;
    protected $state = "AL";
    public    $response;

    public function __construct(HandleGaetway $register, $brand, $_3d = false)
    {


        
        $this->ReqGateway = $register;

        $this->_3d = $_3d;

        $this->setBrandProps($brand);
        $this->setState();
        $this->rgisterFibonatix($brand);
    }

    public function checkBrandExist($brand){

        if(! $brand || ! (array_key_exists($brand, $this->brand))){
            throw new \Exception("You must set brand name");
            return false;
        }
        return true;
    }

    public function setBrandProps(string $brandName = ''){

        if($this->checkBrandExist($brandName)){

            if($this->brand[$brandName]) return;// $this->brand[$brandName];

            $fn = "get" . ucfirst($brandName) . 'Props';
            $this->brand[$brandName] = $this->ReqGateway->$fn($this->_3d);
        }
    }

    public function getBrandProps(string $brandName = ''){

        if($this->checkBrandExist($brandName)){

            if($this->brand[$brandName]) return $this->brand[$brandName];
            $fn = "get" . ucfirst($brandName) . 'Props';
            $this->brand[$brandName] = $this->ReqGateway->$fn($this->_3d);
            return $this->brand[$brandName];
        }
        return false;
    }

    public function rgisterFibonatix($brandName)
    {

        $merchantProps = $this->brand[$brandName]; //($brandName == "cvs")? $this->ReqGateway->getCvsProps(): $this->ReqGateway->getCisProps();

        $groupId = $merchantProps['group_id']; //$this->get3d() ?  $merchantProps['group_id']: '1618';  
        $url = 'https://gate.fibonatix.com/paynet/api/v2/sale/group/' . $groupId;

        $requestFields = ($brandName == "cvs")? $this->getCvsFields(): $this->getCisFields();
        $requestFields['control'] = $this->ReqGateway->signPaymentRequest($requestFields, $groupId, $merchantProps['merchantControl']);
        $resault = $this->ReqGateway->sendRequest($url, $requestFields);
        $this->response = $resault;
    }

    public function setState()
    {
        if (isset($_POST['country']) && strtolower($_POST['country']) == 'ca') {
            $this->state = 'ON';
        } elseif (isset($_POST['country']) && strtolower($_POST['country']) == 'us') {
            $this->state = 'AL';
        } elseif (isset($_POST['country']) && strtolower($_POST['country']) == 'au') {
            $this->state = 'ACT';
        }
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function get3d()
    {
        return $this->_3d;
    }

    public function getCvsFields()
    {
        //$redirect_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]";

        return [
            'client_orderid' => 'canadavisaservices',
            'order_desc' => 'www.canadavisaservices.org Order',
            'first_name' => isset($_POST['first_name']) ? $_POST['first_name'] : '',
            'last_name' => isset($_POST['last_name']) ? $_POST['last_name'] : '',
            'address1' => isset($_POST['address1']) ? $_POST['address1'] : '',
            'city' => isset($_POST['city']) ? $_POST['city'] : '',
            'zip_code' => (isset($_POST['zip_code']) && $_POST['zip_code'] != '') ? $_POST['zip_code'] : '12345',
            'country' => isset($_POST['country']) ? strtoupper($_POST['country']) : "IL",
            'state' => $this->state,
            'phone' => '+100000000',
            'amount' => isset($_POST['amount']) ? $_POST['amount'] : '10.5',
            'email' => isset($_POST['email']) ? $_POST['email'] : '',
            'currency' => !empty($_POST['currency']) ? $_POST['currency'] : 'USD',
            'ipaddress' => $_SERVER["REMOTE_ADDR"],
            'site_url' => 'www.canadavisaservices.org',
            'credit_card_number' => isset($_POST['credit_card_number']) ? str_replace(' ', '', $_POST['credit_card_number']) : '',
            'card_printed_name' => isset($_POST['card_printed_name']) ? $_POST['card_printed_name'] : '',
            'expire_month' => isset($_POST['expire_month']) ? $_POST['expire_month'] : '',
            'expire_year' => isset($_POST['expire_year']) ? $_POST['expire_year'] : '',
            'cvv2' => isset($_POST['cvv2']) ? $_POST['cvv2'] : '',
            'redirect_url' => 'https://www.canadavisaservices.org/payment-successful'
        ];
    }


    public function getCisFields()
    {
        return [
            'client_orderid' => 'CanadaTimeControl',
            'order_desc' => 'www.itscanadatime.com Order',
            'first_name' => isset($_POST['first_name']) ? $_POST['first_name'] : '',
            'last_name' => isset($_POST['last_name']) ? $_POST['last_name'] : '',
            'address1' => isset($_POST['address1']) ? $_POST['address1'] : '',
            'city' => isset($_POST['city']) ? $_POST['city'] : '',
            'zip_code' => (isset($_POST['zip_code']) && $_POST['zip_code'] != '') ? $_POST['zip_code'] : '12345',
            'country' => strtoupper($_POST['country']),
            'state' => $this->state,
            'phone' => '+100000000',
            'amount' => isset($_POST['amount']) ? $_POST['amount'] : '',
            'email' => isset($_POST['email']) ? $_POST['email'] : '',
            'currency' => !empty($_POST['currency']) ? $_POST['currency'] : 'USD',
            'ipaddress' => $_SERVER["REMOTE_ADDR"],
            'site_url' => 'www.itscanadatime.com',
            'credit_card_number' => isset($_POST['credit_card_number']) ? str_replace(' ', '', $_POST['credit_card_number']) : '',
            'card_printed_name' => isset($_POST['card_printed_name']) ? $_POST['card_printed_name'] : '',
            'expire_month' => isset($_POST['expire_month']) ? $_POST['expire_month'] : '',
            'expire_year' => isset($_POST['expire_year']) ? $_POST['expire_year'] : '',
            'cvv2' => isset($_POST['cvv2']) ? $_POST['cvv2'] : '',
            'redirect_url' => 'https://www.itscanadatime.com/payment-successful'
        ];
    }
}