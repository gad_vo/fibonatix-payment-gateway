<?php

/**
 * Plugin Name: payment_plugin - Contact Form Integrator
 * Plugin URI: https://bitbucket.org/yis_team/cfmi/
 * Description: Form integration to fibonatix payment gateway
 * Version: 0.2.0
 * Author: Dima Minka & Beliakov Anton
 * Author URI: https://cdk.co.il/
 * License: MIT License
 */


if(file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ))
    class_exists('App\FibonatixRegiter') || require_once __DIR__ . '/vendor/autoload.php';

if (php_sapi_name() != 'cli' && function_exists('env')) {
    (new App\FibonatixRegiter)->filtersInit();
}


